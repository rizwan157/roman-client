import { Component, OnInit } from '@angular/core';
import { ClientServiceService } from '../client-service.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  constructor(private service: ClientServiceService, private toastr: ToastrService, private router: Router) { }
  model: any = {};
  ngOnInit() {
  }

  onSubmit() {
    console.log(this.model);
    this.service.ResetPassword(this.model)
      .subscribe(res => {
        if (res.status == false)
          this.toastr.error(res.message, "Error");
        else
        {
          this.toastr.success(res.message, "Success");
          this.router.navigateByUrl('/login');
        }
        console.log('res', res);
        // let id = res['_id'];
        // this.router.navigate(['/book-details', id]);
      }, (err) => {
        console.log(err);
      }
      );
  }

}
