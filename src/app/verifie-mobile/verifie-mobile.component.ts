import { Component, OnInit } from '@angular/core';
import { AccountKit, AuthResponse } from 'ng2-account-kit';
import { Router } from "@angular/router"

@Component({
  selector: 'app-verifie-mobile',
  templateUrl: './verifie-mobile.component.html',
  styleUrls: ['./verifie-mobile.component.css']
})
export class VerifieMobileComponent implements OnInit {

    model : any = {};
  private bodyText: string;

  constructor(private router: Router) {
      AccountKit.init({
        appId: '187443785510488',
        state: '6175d77b3b73bfc9f4c9e1b34f460650',
        version: 'v1.1',
        display: "modal"
      });  
  }

    ngOnInit() {
    }

  login(): void {
    AccountKit.login('PHONE', { countryCode: this.model.dail_code , phoneNumber: this.model.mobile_no }).then(
      (response: AuthResponse) => {
        this.router.navigate(['/regiter', [this.model.dail_code, this.model.mobile_no]]);
        // if (response.status == "PARTIALLY_AUTHENTICATED")
      },
      (error: any) => {
        console.error("Error Message: " + error)
      }
    );
  }
}
