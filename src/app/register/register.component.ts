import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ClientServiceService } from '../client-service.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class RegisterComponent implements OnInit {

  model: any = {};
  dail_code: string;
  mobile_no: string;

  constructor(private service: ClientServiceService, private toaster: ToastrService, private route: ActivatedRoute) { 
    this.model.dail_code = this.route.snapshot.params.dail_code;
    this.model.mobile_no = this.route.snapshot.params.mobile_no;
  }
  
  isSubmitted : boolean;

  ngOnInit() {
  }

  onSubmit(){
    this.isSubmitted = true;
    console.log('register',this.model);
    this.service.register(this.model)
      .subscribe(res => {
        if(res.status == true)
          this.toaster.success(res.message, "Success");
        else 
          this.toaster.error(res.message, "Error");

        this.isSubmitted = false;
          // let id = res['_id'];
          // this.router.navigate(['/book-details', id]);
        }, (err) => {
          console.log(err);
        }
      );
  }
}
