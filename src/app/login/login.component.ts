import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ClientServiceService } from '../client-service.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class LoginComponent implements OnInit {

  constructor(private service: ClientServiceService, private toastr: ToastrService, private router: Router) { }
  model: any = {};
  isSubmitted : boolean;

  ngOnInit() {
    // this.toastr.success('Hello world!', 'Toastr fun!');
  }

  onSubmit(){
    this.isSubmitted = true;
        this.service.login(this.model)
      .subscribe(res => {
        if(res.status == false)
          this.toastr.error(res.message, "Error");
        else
          this.toastr.success("Login Success", "Success");

        this.isSubmitted = false;
        console.log('res',res);
          // let id = res['_id'];
          // this.router.navigate(['/book-details', id]);
        }, (err) => {
          console.log(err);
        }
      );
  }

}
