import { Component, OnInit } from '@angular/core';
import { ClientServiceService } from '../client-service.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {

  constructor(private service: ClientServiceService, private toastr: ToastrService, private router: Router) { }

  model: any = {};

  isSubmitted : boolean;

  ngOnInit() {
    // this.toastr.success('Hello world!', 'Toastr fun!');
  }

  onSubmit() {
    this.isSubmitted = true;
    this.service.forgetPassword(this.model)
      .subscribe(res => {
        this.isSubmitted = false;
        if (res.status == false)
          this.toastr.error(res.message, "Error");
        else{

          this.toastr.success(res.message, "Success");
          this.router.navigateByUrl('/reset-password');
        }
        console.log('res', res);
        // let id = res['_id'];
        // this.router.navigate(['/book-details', id]);
      }, (err) => {
        console.log(err);
      }
      );
  }

}
